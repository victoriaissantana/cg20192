#include <stdio.h>
#include <math.h>
#include <GL/glut.h>

int pontoX, pontoY;

void Desenha(void)
{
    glClear(GL_COLOR_BUFFER_BIT);
    glColor3f(0.0, 0.0, 1.0);


    glFlush();
}

void Inicializa(void)
{
    glClearColor(1.0f, 1.0f, 1.0f, 0.0f);
}

int main(int argc, char** argv)
{
    printf("Entre com os pontos X e Y: ");
    scanf("%d", &pontoX);
    scanf("%d", &pontoY);

    glutInit(&argc, argv);
    glutInitDisplayMode(GLUT_SINGLE | GLUT_RGB);
    glutInitWindowSize(320, 240);
    glutCreateWindow("Rasterização");
    glutDisplayFunc(Desenha);
    Inicializa();
    glutMainLoop();
    
}
